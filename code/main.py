import folium
import csv
import pandas as pd
import geopandas as gpd
import numpy as np
from folium.plugins import MarkerCluster
from folium.plugins import StripePattern
from IPython.display import HTML



nom=[]
data_lon=[]
data_lat=[]
poblacio=[]
color=[]
tipus=[]
projecte=[]

# Load the data
y=0
with open("xarxaflu.csv") as rtsd:
    read=csv.reader(rtsd,delimiter=",")
    for i in read:
            nom.append(i[0])
            poblacio.append(i[2])
            data_lon.append(i[7])
            data_lat.append(i[8])
            color.append(i[9])
            tipus.append(i[10])
            projecte.append(i[11])


#for skiping columns name
nom=nom[1:]
data_lon=data_lon[1:]
data_lat=data_lat[1:]
poblacio=poblacio[1:]
color=color[1:]
tipus=tipus[1:]
projecte=projecte[1:]

map = folium.Map([41.4506304,2.0913448], zoom_start=11,tiles="Stamen Toner")
tile = folium.TileLayer('Stamen Terrain').add_to(map)

ly1 = MarkerCluster(name='Xarxa FAIG').add_to(map)
ly2 = MarkerCluster(name='Xarxa TECLA').add_to(map)
ly3 = MarkerCluster(name='Xarxa CENTRINNO').add_to(map)
ly4 = MarkerCluster(name='Xarxa AULA MAR').add_to(map)
ly5 = MarkerCluster(name='Xarxa MAGNET').add_to(map)
ly6 = MarkerCluster(name='Xarxa SENSE-MAKING').add_to(map)
ly7 = MarkerCluster(name='Xarxa REMIX THE SCHOOL DISTRIBUTED').add_to(map)
#ly8 = MarkerCluster(name='Xarxa Espais Maker').add_to(map)
ly9 = MarkerCluster(name='Xarxa DOIT').add_to(map)
ly10 = MarkerCluster(name='Xarxa AMBMAKERS').add_to(map)
ly11 = MarkerCluster(name='BENJAMIN FRANKLIN').add_to(map)
ly12 = MarkerCluster(name='Xarxa FABSCHOOL LA SALLE').add_to(map)

#adding marker and popup of city and crime-name
for i in range(0,len(nom)):
    if projecte[i] in ["FAIG"]:
        folium.Marker(location=[float(data_lon[i]),float(data_lat[i])], icon=folium.Icon(color=color[i],icon_color='#FFFFFF'),popup="<b>"+nom[i]+"</b>\n"+poblacio[i]).add_to(ly1)
 #   elif projecte[i] in ["NETWORK"]:
 #       folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly8)
    elif projecte[i] in ["CENTRINNO"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly3)
    elif projecte[i] in ["SENSE"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly6)
    elif projecte[i] in ["AULAMAR"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly4)
    elif projecte[i] in ["RTSD"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly7)
    elif projecte[i] in ["TECLA"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly2)
    elif projecte[i] in ["MAGNET"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly5)
    elif projecte[i] in ["DOIT"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly9)
    elif projecte[i] in ["AMBMAKERS"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly10)
    elif projecte[i] in ["BENJAMIN"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly11)
    elif projecte[i] in ["FABSCHOOL"]:
        folium.Marker(location=[float(data_lon[i]), float(data_lat[i])],icon=folium.Icon(color=color[i], icon_color='#FFFFFF'),popup="<b>" + nom[i] + "</b>\n" + poblacio[i]).add_to(ly12)


#we can change tiles with help of LayerConrol
folium.LayerControl().add_to(map)

#saving map to a html file
map.save('index.html')
